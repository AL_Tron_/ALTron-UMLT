-- Перевод в одном направлении для одного файла целиком
-- В начале файла дату в формате DD.MM.YYYY
-- направление
-- (скорее всего оригинальный язык можно убрать)
(TypeTrans, Language) ⇒ Language
(ТипПеревода, ИсходныйЯзык) ⇒ КонечныйЯзык
data ListCoordThi = [NumberString]
data ToolTipTrans = ([TypeTrans], String)
data Trans = ([TypeTrans], String, [ToolTip]) -- если опущен [ToolTip] и включена опция в конфиге то в качестве всплывающей подсказки берётся OriginalString
data OriginalStringNumbers = [NumberString] -- строки в которых встречается оригинальный текст
{([THI], OriginalStringNumbers, OriginalString, [Trans])}
-- Если перевод должен быть в всплывающей подсказке
{([THI], OriginalStringNumbers, OriginalString, (OriginalWords, [ToolTipTrans]))}
-- Полный, если нужен контекст
{(ThiAddition, [THI], OriginalStringNumbers, OriginalString, (OriginalWords, [ToolTipTrans]))}
