type Version = String
data SysTrans = Google(G) | Yandex(Y)
data TypeTrans = MinEtymology(ME) | Literal(Ll) | Literary(Lr) | Machine(M) SysTrans | TechnicalCommunity(TC) Version | Technical(Te)
-- data ContextDep = NoContext | Context String - контекст текста
type Thi = Integer
data Language = English | Русский и т. д.
-- (для перевода конкретных строк)
type StringNumber = Integer
type OriginalString = String
-- Номер слова в отрывке для перевода
type WordNumber = Integer
type ToolTipString = String
-- Всплывающие подсказки:
-- Номера слов в переведённом тексте
data TranslateWords = [WordNumber]
-- Номера слов в оригинальном тексте
data OriginalWords = [WordNumber]
data ToolTip = (TranslateWords, OriginalWords) -- пара для всплывающих подсказок
data LocationThi = Before | Mid | After -- положение текущего списка Thi
data ThiAddition = NotNeed | MaybeNeed | Need LocationThi | Need LocationThi ListCoordThi | Need [Thi] --вместо контекста соединяем с предыдущими мысле образами, последний конструктор скорее всего не нужен
-- ListCoordThi определяется для каждого уровня отдельно
